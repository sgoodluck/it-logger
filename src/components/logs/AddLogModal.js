import React, { useState } from 'react';

import M from 'materialize-css/dist/js/materialize.min.js';
import PropTypes from 'prop-types';
import TechSelectOptions from 'components/techs/TechSelectOptions';
import { addLog } from 'actions/logActions';
import { connect } from 'react-redux';

const AddLogModal = ({ addLog }) => {
	const [message, setMessage] = useState('');
	const [attention, setAttention] = useState(false);
	const [tech, setTech] = useState('');

	const onSubmit = () => {
		if (message === '' || tech === '') {
			M.toast({ html: 'Please include a message and technician' });
		} else {
			// Generate payload
			const newLog = {
				message,
				attention,
				tech,
				date: new Date()
			};

			// Dispatch the log
			addLog(newLog);

			// Confirm submission
			M.toast({ html: `Log added by ${tech}` });

			// Reset component level state
			setMessage('');
			setTech('');
			setAttention(false);
		}
	};

	return (
		<div id='add-log-modal' className='modal' style={modalStyle}>
			{' '}
			<div className='modal-content'>
				<h4>Enter System Log</h4>
				<div className='row'>
					<div className='input-field'>
						<input
							type='text'
							name='message'
							value={message}
							onChange={e => setMessage(e.target.value)}
						/>
						<label htmlFor='message' className='active'>
							Log Message
						</label>
					</div>
					<div className='row'>
						<div className='input-field'>
							<select
								name='tech'
								value={tech}
								className='browser-default'
								onChange={e => setTech(e.target.value)}
							>
								<option value='' disabled>
									Select Technician
								</option>
								<TechSelectOptions />
							</select>
						</div>
						<div className='row'>
							<div className='input-field'>
								<p>
									<label>
										<input
											type='checkbox'
											className='filled-in'
											name='checked'
											checked={attention}
											onChange={e => setAttention(!attention)}
										/>
										<span>Needs Attention</span>
									</label>
								</p>
							</div>
						</div>
					</div>
				</div>
				<div className='modal-footer'>
					<a
						href='#!'
						onClick={onSubmit}
						className='modal-close waves-effect blue btn'
					>
						Submit
					</a>
				</div>
			</div>
		</div>
	);
};

const modalStyle = {
	width: '75%',
	height: '75%'
};

AddLogModal.propTypes = {
	addLog: PropTypes.func.isRequired
};

// We are not reading app level state here so mapStateToProps can be null
export default connect(
	null,
	{ addLog }
)(AddLogModal);
