import React, { useEffect } from 'react';

import LogItem from './LogItem';
import Preloader from 'components/layout/Preloader';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { getLogs } from 'actions/logActions';

const Logs = ({ log: { logs, loading }, getLogs }) => {
	useEffect(() => {
		getLogs();
		// eslint-disable-nextline
	}, [getLogs]);

	if (loading || logs === null) {
		return <Preloader />;
	} else {
		return (
			<ul className='collection with-header'>
				<li className='collection-header'>
					<h4 className='center'>System Logs</h4>
				</li>
				{!loading && logs.length === 0 ? (
					<p className='center'>No Logs to show...</p>
				) : (
					logs.map(log => <LogItem key={log.id} log={log} />)
				)}
			</ul>
		);
	}
};

/**
 * This is a typescript-esque module that allows us
 * to specify what props this component should expect.
 *
 * This allows for more robust code and easier debugging
 * if incorrect data types are passed in somehow
 */
Logs.propTypes = {
	log: PropTypes.object.isRequired
};

/**
 *
 * @param {*} state
 * @description: This method is passed into connect and hooks up
 * the app level state "log" available in the root reducer to the
 * props of this particular component.
 */
const mapStateToProps = state => ({
	log: state.log, // references app level state in the reducer!
	getLogs: PropTypes.func.isRequired
});

/**
 * Components connected to app level state need to use this
 * connect(mapStateToProps, { availableActions })(ComponentName);
 * syntax. This allows actions like getLogs, available in the logActions
 * file to be invoked from this component directly
 */
export default connect(
	mapStateToProps,
	{ getLogs }
)(Logs);
