import React, { useEffect, useState } from 'react';
import { getLogs, updateLog } from 'actions/logActions';

import M from 'materialize-css/dist/js/materialize.min.js';
import PropTypes from 'prop-types';
import TechSelectOptions from 'components/techs/TechSelectOptions';
import { connect } from 'react-redux';

const EditLogModal = ({ current, updateLog, getLogs }) => {
	const [message, setMessage] = useState('');
	const [attention, setAttention] = useState(false);
	const [tech, setTech] = useState('');

	useEffect(() => {
		if (current) {
			setMessage(current.message);
			setAttention(current.attention);
			setTech(current.tech);
		}
	}, [current]);

	const onSubmit = () => {
		if (message === '' || tech === '') {
			M.toast({ html: 'Please include a message and technician' });
		} else {
			const updLog = {
				id: current.id,
				message,
				attention,
				tech,
				date: new Date()
			};

			updateLog(updLog);
			getLogs();
			M.toast({ html: `Log updated by ${tech}` });

			setMessage('');
			setTech('');
			setAttention(false);
		}
	};

	return (
		<div id='edit-log-modal' className='modal' style={modalStyle}>
			{' '}
			<div className='modal-content'>
				<h4>Enter System Log</h4>
				<div className='row'>
					<div className='input-field'>
						<input
							type='text'
							name='message'
							value={message}
							onChange={e => setMessage(e.target.value)}
						/>
						{current && (
							<label htmlFor='message' className='active'>
								Log Message
							</label>
						)}
					</div>
					<div className='row'>
						<div className='input-field'>
							<select
								name='tech'
								value={tech}
								className='browser-default'
								onChange={e => setTech(e.target.value)}
							>
								<option value='' disabled>
									Select Technician
								</option>
								<TechSelectOptions />
							</select>
						</div>
						<div className='row'>
							<div className='input-field'>
								<p>
									<label>
										<input
											type='checkbox'
											className='filled-in'
											name='checked'
											checked={attention}
											onChange={e => setAttention(!attention)}
										/>
										<span>Needs Attention</span>
									</label>
								</p>
							</div>
						</div>
					</div>
				</div>
				<div className='modal-footer'>
					<a
						href='#!'
						onClick={onSubmit}
						className='modal-close waves-effect blue btn'
					>
						Submit
					</a>
				</div>
			</div>
		</div>
	);
};

const modalStyle = {
	width: '75%',
	height: '75%'
};

EditLogModal.propTypes = {
	updateLog: PropTypes.func.isRequired,
	getLogs: PropTypes.func.isRequired,
	current: PropTypes.object
};

const mapStateToProps = state => ({
	current: state.log.current
});

// We do want state here since we are going to be reading it
export default connect(
	mapStateToProps,
	{ updateLog, getLogs }
)(EditLogModal);
